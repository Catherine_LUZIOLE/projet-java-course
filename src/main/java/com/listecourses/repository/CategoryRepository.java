package com.listecourses.repository;

import com.listecourses.beans.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository  extends CrudRepository<Category, Long> {

}
